import 'package:flutter/material.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';

class UserProvider with ChangeNotifier {
  UserEntity? _user;

  UserEntity? get user => _user;

  void setUser(UserEntity? user) {
    _user = user;

    notifyListeners();
  }
}

/*
Uso:

En el main:

  import 'package:provider/provider.dart';
  import 'package:prupass_androidstudio/ui/providers/counter_provider.dart';
  void main() {
    // instanciamos el inyector de dependencias
    Register().regist();

    runApp(ChangeNotifierProvider(
      create: (_) => CounterProvider(),
      child: const MyApp(),
    ));
  }

En una pagina que lo quieras usar:
  Acceder a un provider en modo watch para que me notifique los cambios

    import 'package:provider/provider.dart';
    import 'package:prupass_androidstudio/ui/providers/counter_provider.dart';

    context.watch<CounterProvider>()

    ej: 
    Text(context.watch<CounterProvider>(), style: cpmst TextStyle());

  Modificar un provider
    context.read<CounterProvider>().imcrement();

*/
