import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injector/injector.dart';
import 'package:prupass_androidstudio/domain/entities/password_entity.dart';
import 'package:prupass_androidstudio/ui/pages/passwords/blocs/passwords_bloc.dart';
import 'package:prupass_androidstudio/ui/pages/widgets/custom_drawer.dart';
import 'package:prupass_androidstudio/ui/pages/widgets/list_secctions_items_widget.dart';

class ListPasswordsScreen extends StatelessWidget {
  const ListPasswordsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PasswordsBloc _passwordsBloc =
        Injector.appInstance.get<PasswordsBloc>();

    return BlocProvider(
      create: (context) => _passwordsBloc,
      child: _ListPasswordsScreenBody(),
    );
  }
}

class _ListPasswordsScreenBody extends StatefulWidget {
  @override
  _ListPasswordsScreenBodyState createState() =>
      _ListPasswordsScreenBodyState();
}

class _ListPasswordsScreenBodyState extends State<_ListPasswordsScreenBody> {
  late PasswordsBloc _passwordsBloc;
  ListSecctionItemsWidget? listWidget;

  @override
  void initState() {
    super.initState();
    _passwordsBloc = BlocProvider.of<PasswordsBloc>(context);
    _passwordsBloc.add(LoadPasswordsEvent(context: context));
  }

  void refreshList(List<PasswordCategory> passwordCategories) {
    print("llamamos al listWidget?.refreshList();");
    listWidget?.refreshList();
  }

  void onLongPressTile(String id) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('ID del elemento'),
          content: Text('Has mantenido pulsado el elemento con ID: $id'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('Cerrar'),
            ),
          ],
        );
      },
    );
  }

  void onClickTile(String id) {
    _passwordsBloc.add(EditPasswordEvent(id: id, context: context));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Passwords'),
      ),
      body: BlocConsumer<PasswordsBloc, PasswordsState>(
        listener: (context, state) {
          print("Escuchamos a ${state}");
          if (state is PasswordsLoaded) {
            refreshList(state.passwordCategories);
          }
        },
        builder: (context, state) {
          if (state is PasswordsLoading) {
            return const CircularProgressIndicator();
          } else if (state is PasswordsLoaded) {
            print("Refescar");
            listWidget = ListSecctionItemsWidget(
              elementList: state.passwordCategories,
              onLongPressTile: onLongPressTile,
              onClickTile: onClickTile,
            );
            return Container(
              child: listWidget ?? SizedBox.shrink(),
            );
          } else if (state is PasswordsError) {
            return Text(state.message);
          }
          return const SizedBox.shrink();
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.pushNamed(context, '/edit_passwords_screen');
        },
      ),
      drawer: const CustomDrawer(),
    );
  }
}
