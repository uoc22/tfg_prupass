import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prupass_androidstudio/domain/entities/password_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_password_repository.dart';
import 'package:prupass_androidstudio/domain/usecases/passwords/usecases/delete_password_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/passwords/usecases/get_all_passwords_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/passwords/usecases/get_password_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/passwords/usecases/save_password_usecase.dart';
import 'package:prupass_androidstudio/ui/providers/user_provider.dart';

// Eventos
abstract class PasswordsEvent {}

class LoadPasswordsEvent extends PasswordsEvent {
  final BuildContext context;

  LoadPasswordsEvent({required this.context});
}

class EditPasswordEvent extends PasswordsEvent {
  final String id;
  final BuildContext context;

  EditPasswordEvent({required this.id, required this.context});
}

class GetPassword extends PasswordsEvent {
  final String id;
  final BuildContext context;

  GetPassword(this.id, {required this.context});
}

class SavePassword extends PasswordsEvent {
  final String id;
  final String noteName;
  final String selectedFolder;
  final String noteNotes;
  final String noteUrl;
  final String noteUser;
  final String notePass;
  final bool noteFavorite;
  final BuildContext context;

  SavePassword({
    required this.id,
    required this.noteName,
    required this.selectedFolder,
    required this.noteNotes,
    required this.noteUrl,
    required this.noteUser,
    required this.notePass,
    required this.noteFavorite,
    required this.context,
  });
}

class UpdateFavoriteStatus extends PasswordsEvent {
  final bool isFavorite;

  UpdateFavoriteStatus({required this.isFavorite});
}

// Estados
abstract class PasswordsState {}

class PasswordsInitial extends PasswordsState {}

class PasswordsLoading extends PasswordsState {}

class PasswordsLoaded extends PasswordsState {
  final List<PasswordCategory> passwordCategories;

  PasswordsLoaded({required this.passwordCategories});
}

class PasswordLoaded extends PasswordsState {
  final PasswordEntity password;

  PasswordLoaded({required this.password});
}

class PasswordFavoriteChange extends PasswordsState {
  final bool favorite;

  PasswordFavoriteChange({required this.favorite});
}

class PasswordSaving extends PasswordsState {}

class PasswordSaved extends PasswordsState {
  final PasswordEntity password;

  PasswordSaved({required this.password});
}

class PasswordsError extends PasswordsState {
  final String message;

  PasswordsError({required this.message});
}

class PasswordError extends PasswordsState {
  final String message;

  PasswordError({required this.message});
}

// BLoC
class PasswordsBloc extends Bloc<PasswordsEvent, PasswordsState> {
  final AbstractPasswordRepository _repository;
  final GetAllPasswordsUseCase getAllPasswordsUseCase;
  final GetPasswordUseCase getPasswordUseCase;
  final SavePasswordUseCase savePasswordUseCase;
  final DeletePasswordUseCase deletePasswordUseCase;

  PasswordsBloc(
      this._repository,
      this.getAllPasswordsUseCase,
      this.getPasswordUseCase,
      this.savePasswordUseCase,
      this.deletePasswordUseCase)
      : super(PasswordsInitial()) {
    on<LoadPasswordsEvent>(_loadPasswords);
    on<EditPasswordEvent>(_editPasswordEvent);
    on<GetPassword>(_getPassword);
    on<SavePassword>(_savePassword);
    on<UpdateFavoriteStatus>(_updateFavoriteStatus);
  }

  Future<void> _loadPasswords(
      LoadPasswordsEvent event, Emitter<PasswordsState> emit) async {
    emit(PasswordsLoading());
    try {
      List<PasswordEntity> passwords = await getAllPasswordsUseCase
          .getPasswords(event.context.read<UserProvider>().user!);

      Map<String, List<PasswordEntity>> categoryMap = {'favorite': []};

      for (var password in passwords) {
        // Imprime cada nota
        print('Note: ${password.toJson()}');

        if (password.favorite) {
          categoryMap['favorite']!.add(password);
        }
        if (categoryMap.containsKey(password.folder)) {
          categoryMap[password.folder]!.add(password);
        } else {
          categoryMap[password.folder] = [password];
        }
      }

      List<PasswordCategory> passwordCategories = [];
      categoryMap.forEach((key, value) {
        passwordCategories
            .add(PasswordCategory(categoryName: key, items: value));
      });

      print('Emitimos: PasswordsLoaded');
      emit(PasswordsLoaded(passwordCategories: passwordCategories));
    } catch (e) {
      emit(PasswordsError(message: 'Error al cargar las Passwords: $e'));
    }
  }

  Future<void> _editPasswordEvent(
      EditPasswordEvent event, Emitter<PasswordsState> emit) async {
    // Aquí iría el código para manejar la selección de una nota
    print('Editamos el Id: ${event.id}');
    Navigator.pushNamed(event.context, '/edit_passwords_screen',
        arguments: event.id);
  }

  Future<void> _getPassword(
      GetPassword event, Emitter<PasswordsState> emit) async {
    emit(PasswordsLoading());

    try {
      print("Llamamos a _getPassword con id ${event.id}");

      PasswordEntity? password = await getPasswordUseCase.getPassword(
          event.context.read<UserProvider>().user!, event.id);
      if (password != null) {
        emit(PasswordLoaded(password: password));
      } else {
        emit(PasswordError(message: 'Error al cargar la Password'));
      }
    } catch (e) {
      emit(PasswordError(message: 'Error al cargar la Password'));
    }
  }

  Future<void> _savePassword(
      SavePassword event, Emitter<PasswordsState> emit) async {
    emit(PasswordSaving());

    if (event.noteName.isEmpty ||
        event.selectedFolder.isEmpty ||
        event.noteNotes.isEmpty) {
      emit(PasswordError(message: 'Por favor, complete todos los campos'));
    } else {
      emit(PasswordsLoading());
      try {
        PasswordEntity? password = await savePasswordUseCase.savePassword(
            event.context.read<UserProvider>().user!,
            event.id,
            event.noteName,
            event.selectedFolder,
            event.noteFavorite,
            event.noteUrl,
            event.noteUser,
            event.notePass,
            event.noteNotes);

        print(
            "evento: ${event.noteFavorite} Password favorite: ${password!.favorite}");
        if (password != null) {
          emit(PasswordSaved(password: password));
          // Actualizar la pantalla anterior
          Navigator.pushReplacementNamed(
              event.context, '/list_passwords_screen');
        } else {
          emit(PasswordError(message: 'Error al guardar la Password'));
        }
      } catch (e) {
        emit(PasswordError(message: 'Error al guardar la Password'));
      }
    }
  }

  void _updateFavoriteStatus(
      UpdateFavoriteStatus event, Emitter<PasswordsState> emit) {
    emit(PasswordFavoriteChange(favorite: event.isFavorite));
  }
}
