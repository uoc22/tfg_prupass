import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injector/injector.dart';
import 'package:prupass_androidstudio/ui/pages/passwords/blocs/passwords_bloc.dart';

class EditPasswordsScreen extends StatefulWidget {
  const EditPasswordsScreen({Key? key, this.id}) : super(key: key);
  final String? id;

  @override
  _EditPasswordsScreenState createState() => _EditPasswordsScreenState();
}

class _EditPasswordsScreenState extends State<EditPasswordsScreen> {
  final PasswordsBloc _passwordBloc = Injector.appInstance.get<PasswordsBloc>();
  late bool _isPasswordVisible = false;
  bool _isFavorite = false; // Convertir en una variable de estado

  final List<String> _folderOptions = [
    'Email',
    'Trabajo',
    'Juegos',
    'Negocios',
    'Inversion',
  ];

  String? _selectedFolder;

  final TextEditingController _passwordNameController = TextEditingController();
  final TextEditingController _passwordUrlController = TextEditingController();
  final TextEditingController _passwordUserController = TextEditingController();
  final TextEditingController _passwordPassController = TextEditingController();
  final TextEditingController _passwordNotesController =
      TextEditingController();
  final _formKey = GlobalKey<FormState>();

  void _onCreateCategory() {
    // Acción al pulsar el ícono de añadir carpeta
  }

  void _onSavePassword(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      final String passwordName = _passwordNameController.text;
      final String? selectedFolder = _selectedFolder;
      final String passwordNotes = _passwordNotesController.text;
      final String passwordUrl = _passwordUrlController.text;
      final String passwordUser = _passwordUserController.text;
      final String passwordPass = _passwordPassController.text;
      print("Favorite: ${_isFavorite}");
      _passwordBloc.add(SavePassword(
        id: widget.id ?? '',
        noteName: passwordName,
        selectedFolder: selectedFolder!,
        noteNotes: passwordNotes,
        noteUrl: passwordUrl,
        noteUser: passwordUser,
        notePass: passwordPass,
        noteFavorite: _isFavorite,
        context: context,
      ));
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      print("entramos en initstate id ${widget.id}");

      if (widget.id != null) {
        _passwordBloc.add(GetPassword(widget.id!, context: context));
      }
    });
  }

  @override
  void dispose() {
    _passwordNameController.dispose();
    _passwordNotesController.dispose();
    _passwordUrlController.dispose();
    _passwordUserController.dispose();
    _passwordPassController.dispose();
    super.dispose();
  }

  void _togglePasswordVisibility() {
    setState(() {
      _isPasswordVisible = !_isPasswordVisible;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<PasswordsBloc>(
      create: (context) => _passwordBloc,
      child: BlocBuilder<PasswordsBloc, PasswordsState>(
        builder: (context, state) {
          if (state is PasswordLoaded) {
            _passwordNameController.text = state.password.name;
            _passwordNotesController.text = state.password.note;
            _passwordUrlController.text = state.password.url;
            _passwordUserController.text = state.password.user;
            _passwordPassController.text = state.password.pass;
            _selectedFolder = state.password.folder;
            _isFavorite = state.password.favorite;
          }

          if (state is PasswordFavoriteChange) {
            _isFavorite = state.favorite;
          }

          return Scaffold(
            appBar: AppBar(
              title: const Text('Editar password'),
              actions: [
                IconButton(
                  icon: const Icon(Icons.check),
                  onPressed: () => _onSavePassword(context),
                ),
              ],
            ),
            body: Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextFormField(
                        controller: _passwordNameController,
                        decoration: const InputDecoration(labelText: 'Nombre'),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Por favor, ingresa un nombre.';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 16.0),
                      Row(
                        children: [
                          Expanded(
                            child: DropdownButtonFormField<String>(
                              value: _selectedFolder,
                              items: _folderOptions.map((folder) {
                                return DropdownMenuItem(
                                  value: folder,
                                  child: Text(folder),
                                );
                              }).toList(),
                              decoration: const InputDecoration(
                                labelText: 'Carpeta',
                              ),
                              onChanged: (value) {
                                setState(() {
                                  _selectedFolder = value;
                                });
                              },
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Por favor, selecciona una carpeta.';
                                }
                                return null;
                              },
                            ),
                          ),
                          IconButton(
                            icon: const Icon(Icons.add),
                            onPressed: _onCreateCategory,
                          ),
                        ],
                      ),
                      const SizedBox(height: 16.0),
                      TextFormField(
                        controller: _passwordUrlController,
                        decoration: const InputDecoration(labelText: 'URL'),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor, ingresa una URL.';
                          }
                          final uri = Uri.tryParse(value);
                          if (uri == null || !uri.isAbsolute) {
                            return 'Por favor, ingresa una URL válida.';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 16.0),
                      TextFormField(
                        controller: _passwordUserController,
                        decoration: const InputDecoration(labelText: 'Usuario'),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return 'Por favor, ingresa un usuario.';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 16.0),
                      Row(
                        children: [
                          Expanded(
                            child: TextFormField(
                              controller: _passwordPassController,
                              obscureText: !_isPasswordVisible,
                              decoration: InputDecoration(
                                labelText: 'Contraseña',
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    _isPasswordVisible
                                        ? Icons.visibility_off
                                        : Icons.visibility,
                                  ),
                                  onPressed: _togglePasswordVisibility,
                                ),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Por favor, ingresa una contraseña.';
                                }
                                return null;
                              },
                            ),
                          ),
                          IconButton(
                            icon: const Icon(Icons.refresh),
                            onPressed: () {
                              // Acción al pulsar el ícono de refrescar contraseña
                            },
                          ),
                        ],
                      ),
                      const SizedBox(height: 16.0),
                      TextFormField(
                        controller: _passwordNotesController,
                        minLines: 3,
                        maxLines: null,
                        decoration: const InputDecoration(
                          labelText: 'Notas',
                        ),
                      ),
                      const SizedBox(height: 16.0),
                      Row(
                        children: [
                          const Text('Favorito'),
                          const SizedBox(width: 8.0),
                          Switch(
                            value: _isFavorite,
                            onChanged: (value) {
                              _passwordBloc
                                  .add(UpdateFavoriteStatus(isFavorite: value));
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
