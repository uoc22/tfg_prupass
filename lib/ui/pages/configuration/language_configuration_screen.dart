import 'package:flutter/material.dart';

class LanguageConfigurationScreen extends StatelessWidget {
  const LanguageConfigurationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Idioma'),
        actions: [
          IconButton(
            icon: const Icon(Icons.check),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: DropdownButtonFormField<String>(
              value: 'Español',
              items: const [
                DropdownMenuItem<String>(
                  value: 'Español',
                  child: Text('Español'),
                ),
              ],
              decoration: const InputDecoration(
                labelText: 'Seleccione un idioma',
              ),
              onChanged: (value) {},
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(60.0),
            child: Text(
              'De momento solo se dispone de un único idioma.\nEn el futuro se añadirán Inglés y Francés.',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
