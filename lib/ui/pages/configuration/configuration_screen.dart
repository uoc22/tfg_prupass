import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:prupass_androidstudio/ui/providers/user_provider.dart';

class ConfigurationScreen extends StatelessWidget {
  const ConfigurationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final userEmail = context.read<UserProvider>().user?.email;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Configuración'),
      ),
      body: ListView(
        children: [
          ListTile(
            title: const Text('Cuenta de PruPass:'),
            subtitle: Text(userEmail ?? ''),
            onTap: () {
              Navigator.pushNamed(context, '/account_configuration_screen');
            },
          ),
          ListTile(
            title: const Text('Seguridad'),
            onTap: () {
              Navigator.pushNamed(context, '/security_configuration_screen');
            },
          ),
          ListTile(
            title: const Text('Idioma'),
            onTap: () {
              Navigator.pushNamed(context, '/language_configuration_screen');
            },
          ),
          ListTile(
            title: const Text('Cerrar sesión'),
            onTap: () {
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  title: const Text('Confirmación'),
                  content:
                      const Text('¿Estás seguro de que deseas cerrar sesión?'),
                  actions: [
                    TextButton(
                      child: const Text('Cancelar'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                    TextButton(
                      child: const Text('Cerrar sesión'),
                      onPressed: () {
                        // Aquí puedes agregar la lógica para cerrar sesión
                        context.read<UserProvider>().setUser(null);

                        // Volver al home
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            '/home_screen', (Route<dynamic> route) => false);
                      },
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
