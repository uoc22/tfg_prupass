import 'package:flutter/material.dart';

class SecurityConfigurationScreen extends StatelessWidget {
  const SecurityConfigurationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Seguridad'),
      ),
      body: const Padding(
        padding: EdgeInsets.all(16.0),
        child: Center(
          child: Text(
            'De momento no existen opciones de configuración.\nCuando las necesitemos, implementaremos esta pantalla para gestionarlas.',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 18,
            ),
          ),
        ),
      ),
    );
  }
}
