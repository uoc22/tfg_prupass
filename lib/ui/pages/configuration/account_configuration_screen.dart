import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:prupass_androidstudio/ui/providers/user_provider.dart';

class AccountConfigurationScreen extends StatelessWidget {
  const AccountConfigurationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userProvider = context.read<UserProvider>();
    final user = userProvider.user;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Cuenta'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              title: const Text(
                'Información del usuario',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('Email: ${user?.email ?? ""}'),
                  Text('Nombre: ${user?.name ?? ""}'),
                  Text('Apellido: ${user?.lastname ?? ""}'),
                  Text('Dirección: ${user?.address ?? ""}'),
                  Text('Teléfono: ${user?.phone ?? ""}'),
                ],
              ),
            ),
            const Divider(), // Línea de separación
            const SizedBox(height: 16),
            const ListTile(
              title: Text(
                'Origen de datos',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              subtitle: Text(
                'Firestore',
                style: TextStyle(
                  fontSize: 14,
                ),
              ),
            ),
            const Divider(), // Línea de separación
            const SizedBox(height: 16),
            ListTile(
              title: const Text(
                'Cambiar contraseña maestra',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              enabled: false,
              tileColor: Colors.grey[200],
            ),
          ],
        ),
      ),
    );
  }
}
