import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:prupass_androidstudio/data/datasources/firestore.dart';
import 'package:prupass_androidstudio/firebase_options.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<void> initializeFirebase() async {
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
  }

  @override
  Widget build(BuildContext context) {
    initializeFirebase(); // Inicializa Firebase al construir la página

    return Scaffold(
        appBar: AppBar(
          title: Text('Gestor de contraseñas - Prupass 44'),
        ),
        body: FutureBuilder(
            future: getUsers(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: snapshot.data?.length,
                    itemBuilder: (context, index) {
                      return Text(snapshot.data?[index]['name']);
                    });
              } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            }));
  }
}
