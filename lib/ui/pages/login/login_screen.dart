import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injector/injector.dart';
import 'package:prupass_androidstudio/ui/pages/login/blocs/user_bloc.dart';
import 'package:prupass_androidstudio/ui/pages/login/create_user_screen.dart';
import 'package:prupass_androidstudio/ui/providers/user_provider.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Login')),
      body: SingleChildScrollView(
        // Agregado el SingleChildScrollView
        child: BlocProvider(
          create: (context) => Injector.appInstance.get<UserBloc>(),
          child: const LoginForm(),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const CreateUserScreen(),
              ));
        },
      ),
    );
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  late UserBloc _userBloc;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _isLoggingIn = false;

  @override
  void initState() {
    super.initState();
    _userBloc = BlocProvider.of<UserBloc>(context);
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _userBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            child: Image.asset(
              'assets/logo.png',
              width: 200,
              height: 200,
            ),
          ),
          const SizedBox(height: 16.0),
          Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                TextFormField(
                  controller: _emailController,
                  decoration:
                      const InputDecoration(labelText: 'Email de usuario'),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Por favor, ingresa el email de tu cuenta.';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  controller: _passwordController,
                  decoration: const InputDecoration(labelText: 'Contraseña'),
                  obscureText: true,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Por favor, ingresa tu contraseña.';
                    }
                    return null;
                  },
                ),
                const SizedBox(height: 16.0),
                ElevatedButton(
                  onPressed: _isLoggingIn ? null : _loginPressed,
                  child: _isLoggingIn
                      ? const CircularProgressIndicator()
                      : const Text('Iniciar sesión'),
                ),
                BlocListener<UserBloc, UserState>(
                  listener: (context, state) {
                    if (state is UserLoggedIn) {
                      // Instancio mi user y puedo cerrar
                      context.read<UserProvider>().setUser(state.user);

                      // Volver al home
                      Navigator.pushNamed(context, '/home_screen');
                    } else if (state is UserError) {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          content: Text(state.message),
                          backgroundColor: Colors.red,
                        ),
                      );
                      setState(() {
                        _isLoggingIn = false;
                      });
                    }
                  },
                  child: const SizedBox.shrink(),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _loginPressed() {
    if (_formKey.currentState!.validate()) {
      final email = _emailController.text;
      final password = _passwordController.text;
      _userBloc.add(LoginButtonPressed(email: email, password: password));

      setState(() {
        _isLoggingIn = true;
      });
    }
  }
}
