import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_user_repository.dart';
import 'package:prupass_androidstudio/domain/usecases/login/usecases/create_user_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/login/usecases/get_user_usecase.dart';

// Eventos
abstract class UserEvent {}

class LoginButtonPressed extends UserEvent {
  final String email;
  final String password;

  LoginButtonPressed({required this.email, required this.password});
}

class CreateUserButtonPressed extends UserEvent {
  final String email;
  final String password;
  final String username;

  final String lastname;
  final String? address;
  final String? phone;

  CreateUserButtonPressed(
      {required this.email,
      required this.password,
      required this.username,
      required this.lastname,
      required this.address,
      required this.phone});
}

// Estados
abstract class UserState {}

class UserInitial extends UserState {}

class UserLoading extends UserState {}

class UserLoggedIn extends UserState {
  final UserEntity user;

  UserLoggedIn({required this.user});
}

class UserCreated extends UserState {
  final UserEntity user;

  UserCreated({required this.user});
}

class UserError extends UserState {
  final String message;

  UserError({required this.message});
}

// BLoC
class UserBloc extends Bloc<UserEvent, UserState> {
  final AbstractUserRepository userRepository;
  final CreateUserUseCase createUserUseCase;
  final GetUserUseCase getUserUseCase;

  UserBloc(this.userRepository, this.createUserUseCase, this.getUserUseCase)
      : super(UserInitial()) {
    on<LoginButtonPressed>(_loginButtonPressed);
    on<CreateUserButtonPressed>(_createUserButtonPressed);
  }

  Future<void> _loginButtonPressed(
      LoginButtonPressed event, Emitter<UserState> emit) async {
    if (event.email.isEmpty || event.password.isEmpty) {
      emit(UserError(message: 'Por favor, complete todos los campos'));
    } else {
      emit(UserLoading());
      try {
        UserEntity? user =
            await getUserUseCase.getUser(event.email, event.password);

        if (user != null) {
          emit(UserLoggedIn(user: user));
        } else {
          emit(UserError(message: 'Email o contraseña incorrectos'));
        }
      } catch (e) {
        emit(UserError(message: 'Error al iniciar sesión'));
      }
    }
  }

  Future<void> _createUserButtonPressed(
      CreateUserButtonPressed event, Emitter<UserState> emit) async {
    if (event.email.isEmpty ||
        event.password.isEmpty ||
        event.username.isEmpty) {
      emit(UserError(message: 'Por favor, complete todos los campos'));
    } else {
      emit(UserLoading());
      try {
        UserEntity? user = await createUserUseCase.saveUser(
            event.email,
            event.password,
            event.username,
            event.lastname,
            event.address,
            event.phone);
        if (user != null) {
          emit(UserCreated(user: user));
        } else {
          emit(UserError(message: 'Error al crear usuario'));
        }
      } catch (e) {
        emit(UserError(message: 'Error al crear usuario'));
      }
    }
  }
}
