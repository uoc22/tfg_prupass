import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injector/injector.dart';
import 'package:prupass_androidstudio/ui/pages/login/blocs/user_bloc.dart';

class CreateUserScreen extends StatelessWidget {
  const CreateUserScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Crear Usuario')),
      body: BlocProvider(
        create: (context) => Injector.appInstance.get<UserBloc>(),
        child: const SingleChildScrollView(
          child: CreateUserForm(),
        ),
      ),
      resizeToAvoidBottomInset: false,
    );
  }
}

class CreateUserForm extends StatefulWidget {
  const CreateUserForm({super.key});

  @override
  _CreateUserFormState createState() => _CreateUserFormState();
}

class _CreateUserFormState extends State<CreateUserForm> {
  late UserBloc _userBloc;
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _repeatPasswordController =
      TextEditingController();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _lastnameController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _phoneController = TextEditingController();
  bool _isSaving = false;

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    _repeatPasswordController.dispose();
    _usernameController.dispose();
    _lastnameController.dispose();
    _addressController.dispose();
    _phoneController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _userBloc = BlocProvider.of<UserBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            TextFormField(
              controller: _emailController,
              decoration:
                  const InputDecoration(labelText: 'Correo electrónico'),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Por favor, ingresa el correo electrónico.';
                }
                if (!isValidEmail(value)) {
                  return 'Por favor, ingresa un correo electrónico válido.';
                }
                return null;
              },
            ),
            TextFormField(
              controller: _passwordController,
              decoration: const InputDecoration(labelText: 'Contraseña'),
              obscureText: true,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Por favor, ingresa la contraseña.';
                }
                return null;
              },
            ),
            TextFormField(
              controller: _repeatPasswordController,
              decoration:
                  const InputDecoration(labelText: 'Repetir contraseña'),
              obscureText: true,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Por favor, repite la contraseña.';
                }
                if (value != _passwordController.text) {
                  return 'Las contraseñas no coinciden.';
                }
                return null;
              },
            ),
            TextFormField(
              controller: _usernameController,
              decoration: const InputDecoration(labelText: 'Nombre de usuario'),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Por favor, ingresa el nombre de usuario.';
                }
                return null;
              },
            ),
            TextFormField(
              controller: _lastnameController,
              decoration: const InputDecoration(labelText: 'Apellidos'),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Por favor, ingresa los apellidos.';
                }
                return null;
              },
            ),
            TextFormField(
              controller: _addressController,
              decoration: const InputDecoration(labelText: 'Dirección'),
            ),
            TextFormField(
              controller: _phoneController,
              decoration: const InputDecoration(labelText: 'Teléfono'),
            ),
            const SizedBox(height: 16.0),
            ElevatedButton(
              onPressed: _isSaving ? null : _saveUser,
              child: _isSaving
                  ? const CircularProgressIndicator()
                  : const Text('Crear usuario'),
            ),
            BlocListener<UserBloc, UserState>(
              listener: (context, state) {
                if (state is UserCreated) {
                  Navigator.pop(
                      context); // Cerrar la ventana y volver a la anterior
                } else if (state is UserError) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(state.message),
                      backgroundColor: Colors.red,
                    ),
                  );

                  setState(() {
                    _isSaving = false;
                  });
                }
              },
              child: const SizedBox.shrink(),
            ),
          ],
        ),
      ),
    );
  }

  void _saveUser() {
    if (_formKey.currentState!.validate()) {
      final email = _emailController.text;
      final password = _passwordController.text;
      final username = _usernameController.text;
      final lastname = _lastnameController.text;
      final address = _addressController.text;
      final phone = _phoneController.text;

      _userBloc.add(CreateUserButtonPressed(
        email: email,
        password: password,
        username: username,
        lastname: lastname,
        address: address,
        phone: phone,
      ));

      setState(() {
        _isSaving = true;
      });
    }
  }

  bool isValidEmail(String email) {
    const emailRegex = r'^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$';
    final regExp = RegExp(emailRegex);
    return regExp.hasMatch(email);
  }
}
