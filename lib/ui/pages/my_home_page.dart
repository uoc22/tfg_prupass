import 'dart:developer';

import 'package:encrypt/encrypt.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:injector/injector.dart';
import 'package:provider/provider.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_crypto_repository.dart';
import 'package:prupass_androidstudio/firebase_options.dart';
import 'package:prupass_androidstudio/data/repositories/crypto_repository_adapter.dart';
import "package:pointycastle/pointycastle.dart";
import 'package:prupass_androidstudio/ui/pages/login/login_screen.dart';
import 'package:prupass_androidstudio/ui/providers/user_provider.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<void> initializeFirebase() async {
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
  }

  Future<String> encryptAndDecryptText() async {
    final cr = Injector.appInstance.get<AbstractCrytoRepository>();
    final serviceCrypto = CryptoRepositoryAdapter(cr);

    // Generar par de claves
    final keyPair = serviceCrypto.generateKeyPair();
    final publicKey = keyPair.publicKey;
    final privateKey = keyPair.privateKey;

    // Texto original
    const plaintext = '¡Hola, mundo!';

    // Cifrar utilizando clave pública
    final encrypted = serviceCrypto.encryptPublicKey(plaintext, publicKey);

    // Descifrar utilizando clave privada
    final decrypted = serviceCrypto.decryptPrivateKey(encrypted, privateKey);

    final String publicKeyString =
        serviceCrypto.publicKeyToPem(publicKey as RSAPublicKey);
    final String privateKeyString =
        serviceCrypto.privateKeyToPem(privateKey as RSAPrivateKey);

    final publicKey2 = serviceCrypto.publicKeyFromPem(publicKeyString);
    final privateKey2 = serviceCrypto.privateKeyFromPem(privateKeyString);

    // Descifrar utilizando clave privada
    final decrypted2 = serviceCrypto.decryptPrivateKey(encrypted, privateKey2);
    final encrypted3 = serviceCrypto.encryptPublicKey(plaintext, publicKey2);
    final decrypted3 = serviceCrypto.decryptPrivateKey(encrypted3, privateKey);

    final dp = serviceCrypto.getDerivedPass('PATATA');
    final encrypted12 = serviceCrypto.encrypt(plaintext, dp);
    final decrypted12 = serviceCrypto.decrypt(encrypted12, dp);

    return "Plain text: " +
        plaintext +
        '\n' +
        "Texto cifrado: " +
        encrypted12 +
        "\nPlain text: " +
        decrypted12 +
        '\n' +
        '\n' +
        '\n' +
        "Plain text: " +
        plaintext +
        '\n' +
        "Texto cifrado: " +
        encrypted +
        "\nPlain text: " +
        decrypted +
        "\nPlain text from privatekey PEM: " +
        decrypted2 +
        "\nPlain text from publickey PEM: " +
        decrypted3;
  }

  @override
  Widget build(BuildContext context) {
    initializeFirebase(); // Inicializa Firebase al construir la página

    return Scaffold(
      appBar: AppBar(
        title: Text('Gestor de contraseñas - Prupass 11'),
      ),
      body: FutureBuilder<String>(
          future: encryptAndDecryptText(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Center(
                child: Text(snapshot.data!),
              );
            } else if (snapshot.hasError) {
              return Center(
                child: Text('Error: ${snapshot.error}'),
              );
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => LoginScreen(),
            ),
          ).then((value) {
            UserEntity? user = context.read<UserProvider>().user;
            String name = '';
            if (user != null) {
              name = user.name;
            }
            log('Se ha cerrado la pantalla de login. user: $name');
          });
        },
      ),
    );
  }
}
