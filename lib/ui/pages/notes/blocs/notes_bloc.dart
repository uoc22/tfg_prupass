import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:prupass_androidstudio/domain/entities/note_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_note_repository.dart';
import 'package:prupass_androidstudio/domain/usecases/notes/usecases/delete_note_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/notes/usecases/get_all_notes_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/notes/usecases/get_note_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/notes/usecases/save_note_usecase.dart';
import 'package:prupass_androidstudio/ui/providers/user_provider.dart';

// Eventos
abstract class NotesEvent {}

class LoadNotesEvent extends NotesEvent {
  final BuildContext context;

  LoadNotesEvent({required this.context});
}

class EditNoteEvent extends NotesEvent {
  final String noteId;
  final BuildContext context;

  EditNoteEvent({required this.noteId, required this.context});
}

class GetNote extends NotesEvent {
  final String noteId;
  final BuildContext context;

  GetNote(this.noteId, {required this.context});
}

class SaveNote extends NotesEvent {
  final String noteId;
  final String noteName;
  final String selectedFolder;
  final String noteNotes;
  final bool noteFavorite;
  final BuildContext context;

  SaveNote({
    required this.noteId,
    required this.noteName,
    required this.selectedFolder,
    required this.noteNotes,
    required this.noteFavorite,
    required this.context,
  });
}

class UpdateFavoriteStatus extends NotesEvent {
  final bool isFavorite;

  UpdateFavoriteStatus({required this.isFavorite});
}

// Estados
abstract class NotesState {}

class NotesInitial extends NotesState {}

class NotesLoading extends NotesState {}

class NotesLoaded extends NotesState {
  final List<NoteCategory> noteCategories;

  NotesLoaded({required this.noteCategories});
}

class NoteLoaded extends NotesState {
  final NoteEntity note;

  NoteLoaded({required this.note});
}

class NoteFavoriteChange extends NotesState {
  final bool favorite;

  NoteFavoriteChange({required this.favorite});
}

class NoteSaving extends NotesState {}

class NoteSaved extends NotesState {
  final NoteEntity note;

  NoteSaved({required this.note});
}

class NotesError extends NotesState {
  final String message;

  NotesError({required this.message});
}

class NoteError extends NotesState {
  final String message;

  NoteError({required this.message});
}

// BLoC
class NotesBloc extends Bloc<NotesEvent, NotesState> {
  final AbstractNoteRepository _repository;
  final GetAllNotesUseCase getAllNotesUseCase;
  final GetNoteUseCase getNoteUseCase;
  final SaveNoteUseCase saveNoteUseCase;
  final DeleteNoteUseCase deleteNoteUseCase;

  NotesBloc(this._repository, this.getAllNotesUseCase, this.getNoteUseCase,
      this.saveNoteUseCase, this.deleteNoteUseCase)
      : super(NotesInitial()) {
    on<LoadNotesEvent>(_loadNotes);
    on<EditNoteEvent>(_editNoteEvent);
    on<GetNote>(_getNote);
    on<SaveNote>(_saveNote);
    on<UpdateFavoriteStatus>(_updateFavoriteStatus);
  }

  Future<void> _loadNotes(
      LoadNotesEvent event, Emitter<NotesState> emit) async {
    emit(NotesLoading());
    try {
      List<NoteEntity> notes = await getAllNotesUseCase
          .getNotes(event.context.read<UserProvider>().user!);

      Map<String, List<NoteEntity>> categoryMap = {'favorite': []};

      for (var note in notes) {
        // Imprime cada nota
        print('Note: ${note.toJson()}');

        if (note.favorite) {
          categoryMap['favorite']!.add(note);
        }
        if (categoryMap.containsKey(note.folder)) {
          categoryMap[note.folder]!.add(note);
        } else {
          categoryMap[note.folder] = [note];
        }
      }

      List<NoteCategory> noteCategories = [];
      categoryMap.forEach((key, value) {
        noteCategories.add(NoteCategory(categoryName: key, items: value));
      });

      print('Emitimos: NotesLoaded');
      emit(NotesLoaded(noteCategories: noteCategories));
    } catch (e) {
      emit(NotesError(message: 'Error al cargar las notas: $e'));
    }
  }

  Future<void> _editNoteEvent(
      EditNoteEvent event, Emitter<NotesState> emit) async {
    // Aquí iría el código para manejar la selección de una nota
    print('Editamos el Id: ${event.noteId}');
    Navigator.pushNamed(event.context, '/edit_notes_screen',
        arguments: event.noteId);
  }

  Future<void> _getNote(GetNote event, Emitter<NotesState> emit) async {
    emit(NotesLoading());

    try {
      NoteEntity? note = await getNoteUseCase.getNote(
          event.context.read<UserProvider>().user!, event.noteId);
      if (note != null) {
        emit(NoteLoaded(note: note));
      } else {
        emit(NoteError(message: 'Error al cargar la nota'));
      }
    } catch (e) {
      emit(NoteError(message: 'Error al cargar la nota'));
    }
  }

  Future<void> _saveNote(SaveNote event, Emitter<NotesState> emit) async {
    emit(NoteSaving());

    if (event.noteName.isEmpty ||
        event.selectedFolder.isEmpty ||
        event.noteNotes.isEmpty) {
      emit(NoteError(message: 'Por favor, complete todos los campos'));
    } else {
      emit(NotesLoading());
      try {
        NoteEntity? note = await saveNoteUseCase.saveNote(
            event.context.read<UserProvider>().user!,
            event.noteId,
            event.noteName,
            event.selectedFolder,
            event.noteFavorite,
            event.noteNotes);

        print("evento: ${event.noteFavorite} note favorite: ${note!.favorite}");
        if (note != null) {
          emit(NoteSaved(note: note));
          // Actualizar la pantalla anterior
          Navigator.pushReplacementNamed(event.context, '/list_notes_screen');
        } else {
          emit(NoteError(message: 'Error al guardar la nota'));
        }
      } catch (e) {
        emit(NoteError(message: 'Error al guardar la nota'));
      }
    }
  }

  void _updateFavoriteStatus(
      UpdateFavoriteStatus event, Emitter<NotesState> emit) {
    print("emit NoteFavoriteChange: ${event.isFavorite}");
    emit(NoteFavoriteChange(favorite: event.isFavorite));
  }
}
