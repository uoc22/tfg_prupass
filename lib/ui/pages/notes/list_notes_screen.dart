import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injector/injector.dart';
import 'package:prupass_androidstudio/domain/entities/note_entity.dart';
import 'package:prupass_androidstudio/ui/pages/notes/blocs/notes_bloc.dart';
import 'package:prupass_androidstudio/ui/pages/widgets/custom_drawer.dart';
import 'package:prupass_androidstudio/ui/pages/widgets/list_secctions_items_widget.dart';

class ListNotesScreen extends StatelessWidget {
  const ListNotesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final NotesBloc _notesBloc = Injector.appInstance.get<NotesBloc>();

    return BlocProvider(
      create: (context) => _notesBloc,
      child: _ListNotesScreenBody(),
    );
  }
}

class _ListNotesScreenBody extends StatefulWidget {
  @override
  _ListNotesScreenBodyState createState() => _ListNotesScreenBodyState();
}

class _ListNotesScreenBodyState extends State<_ListNotesScreenBody> {
  late NotesBloc _notesBloc;
  ListSecctionItemsWidget? listWidget;

  @override
  void initState() {
    super.initState();
    _notesBloc = BlocProvider.of<NotesBloc>(context);
    _notesBloc.add(LoadNotesEvent(context: context));
  }

  void refreshList(List<NoteCategory> noteCategories) {
    print("llamamos al listWidget?.refreshList();");
    listWidget?.refreshList();
  }

  void onLongPressTile(String id) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text('ID del elemento'),
          content: Text('Has mantenido pulsado el elemento con ID: $id'),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text('Cerrar'),
            ),
          ],
        );
      },
    );
  }

  void onClickTile(String id) {
    _notesBloc.add(EditNoteEvent(noteId: id, context: context));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Notas'),
      ),
      body: BlocConsumer<NotesBloc, NotesState>(
        listener: (context, state) {
          print("Escuchamos a ${state}");
          if (state is NotesLoaded) {
            refreshList(state.noteCategories);
          }
        },
        builder: (context, state) {
          if (state is NotesLoading) {
            return const CircularProgressIndicator();
          } else if (state is NotesLoaded) {
            print("Refescar");
            listWidget = ListSecctionItemsWidget(
              elementList: state.noteCategories,
              onLongPressTile: onLongPressTile,
              onClickTile: onClickTile,
            );
            return Container(
              child: listWidget ?? SizedBox.shrink(),
            );
          } else if (state is NotesError) {
            return Text(state.message);
          }
          return const SizedBox.shrink();
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          Navigator.pushNamed(context, '/edit_notes_screen');
        },
      ),
      drawer: const CustomDrawer(),
    );
  }

}
