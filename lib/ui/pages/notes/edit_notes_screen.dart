import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injector/injector.dart';
import 'package:prupass_androidstudio/ui/pages/notes/blocs/notes_bloc.dart';

class EditNotesScreen extends StatefulWidget {
  const EditNotesScreen({Key? key, this.noteId}) : super(key: key);
  final String? noteId;

  @override
  _EditNotesScreenState createState() => _EditNotesScreenState();
}

class _EditNotesScreenState extends State<EditNotesScreen> {
  final NotesBloc _noteBloc = Injector.appInstance.get<NotesBloc>();
  bool _isFavorite = false; // Convertir en una variable de estado

  final List<String> _folderOptions = [
    'Email',
    'Trabajo',
    'Juegos',
    'Negocios',
    'Inversion',
  ];

  String? _selectedFolder;
  final TextEditingController _noteNameController = TextEditingController();
  final TextEditingController _noteNotesController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  void _onCreateCategory() {
    // Acción al pulsar el ícono de añadir carpeta
  }

  void _onSaveNote(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      final String noteName = _noteNameController.text;
      final String? selectedFolder = _selectedFolder;
      final String noteNotes = _noteNotesController.text;
      print("Favorite: $_isFavorite");
      _noteBloc.add(SaveNote(
        noteId: widget.noteId ?? '',
        noteName: noteName,
        selectedFolder: selectedFolder!,
        noteNotes: noteNotes,
        noteFavorite: _isFavorite,
        context: context,
      ));
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      print("entramos en initstate id ${widget.noteId}");
      if (widget.noteId != null) {
        _noteBloc.add(GetNote(widget.noteId!, context: context));
      }
    });
  }

  @override
  void dispose() {
    _noteNameController.dispose();
    _noteNotesController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<NotesBloc>(
      create: (context) => _noteBloc,
      child: BlocBuilder<NotesBloc, NotesState>(
        builder: (context, state) {
          if (state is NoteLoaded) {
            _noteNameController.text = state.note.name;
            _noteNotesController.text = state.note.note;
            _selectedFolder = state.note.folder;
            _isFavorite = state.note.favorite;
          }

          if (state is NoteFavoriteChange) {
            print("NoteFavoriteChange: ${state.favorite}");
            _isFavorite = state.favorite;
          }

          return Scaffold(
            appBar: AppBar(
              title: const Text('Editar nota'),
              actions: [
                IconButton(
                  icon: const Icon(Icons.check),
                  onPressed: () => _onSaveNote(context),
                ),
              ],
            ),
            body: Form(
              key: _formKey,
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextFormField(
                        controller: _noteNameController,
                        decoration: const InputDecoration(labelText: 'Nombre'),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Por favor, ingresa un nombre.';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 16.0),
                      Row(
                        children: [
                          Expanded(
                            child: DropdownButtonFormField<String>(
                              value: _selectedFolder,
                              items: _folderOptions.map((folder) {
                                return DropdownMenuItem(
                                  value: folder,
                                  child: Text(folder),
                                );
                              }).toList(),
                              decoration: const InputDecoration(
                                labelText: 'Carpeta',
                              ),
                              onChanged: (value) {
                                setState(() {
                                  _selectedFolder = value;
                                });
                              },
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return 'Por favor, selecciona una carpeta.';
                                }
                                return null;
                              },
                            ),
                          ),
                          IconButton(
                            icon: const Icon(Icons.add),
                            onPressed: _onCreateCategory,
                          ),
                        ],
                      ),
                      const SizedBox(height: 16.0),
                      TextFormField(
                        controller: _noteNotesController,
                        minLines: 3,
                        maxLines: null,
                        decoration: const InputDecoration(
                          labelText: 'Notas',
                        ),
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Por favor, ingresa las notas.';
                          }
                          return null;
                        },
                      ),
                      const SizedBox(height: 16.0),
                      Row(
                        children: [
                          const Text('Favorito'),
                          const SizedBox(width: 8.0),
                          Switch(
                            value: _isFavorite,
                            onChanged: (value) {
                              _noteBloc
                                  .add(UpdateFavoriteStatus(isFavorite: value));
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
