import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:prupass_androidstudio/firebase_options.dart';
import 'package:prupass_androidstudio/ui/pages/widgets/custom_drawer.dart';
import 'package:prupass_androidstudio/ui/providers/user_provider.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  Future<void> initializeFirebase() async {
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
  }

  @override
  Widget build(BuildContext context) {
    //print("inicializamos firebase");
    //initializeFirebase(); // Inicializa Firebase al construir la página
    //print("firebase iniciado");
    final userProvider = context.read<UserProvider>();

    if (userProvider.user == null) {
      // Si el usuario es nulo, redirige a la pantalla de inicio de sesión
      WidgetsBinding.instance?.addPostFrameCallback((_) {
        Navigator.pushReplacementNamed(context, '/login_screen');
      });
    } else {
      // Si el usuario no es nulo, redirige a la pantalla de contraseñas
      WidgetsBinding.instance?.addPostFrameCallback((_) {
        Navigator.pushReplacementNamed(context, '/list_notes_screen');
      });
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: const [
            Text(
              'Contenido del body',
              style: TextStyle(fontSize: 20),
            ),
          ],
        ),
      ),
      drawer: const CustomDrawer(),
    );
  }
}
