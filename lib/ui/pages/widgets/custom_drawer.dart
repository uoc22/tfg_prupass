import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:prupass_androidstudio/ui/providers/user_provider.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userProvider = context.read<UserProvider>();
    final user = userProvider.user;

    return Drawer(
      child: Column(
        children: [
          DrawerHeader(
            decoration: const BoxDecoration(
              color: Colors.blue,
            ),
            child: Row(
              children: [
                Text(
                  'Cuenta de ${user?.name ?? ""} (${user?.email ?? ""})',
                  style: const TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          ListTile(
            leading: const Icon(Icons.lock),
            title: const Text('Contraseñas'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/list_passwords_screen');
            },
          ),
          ListTile(
            leading: const Icon(Icons.note),
            title: const Text('Notas'),
            onTap: () {
              Navigator.pushReplacementNamed(context, '/list_notes_screen');
            },
          ),
          const Divider(),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: ListTile(
                title: const Text('Configuración'),
                leading: const Icon(Icons.settings),
                onTap: () {
                  Navigator.pushNamed(context, '/configuration_screen');
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
