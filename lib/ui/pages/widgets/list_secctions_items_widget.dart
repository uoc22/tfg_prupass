import 'package:flutter/material.dart';

class ListSecctionItemsWidget extends StatefulWidget {
  final List<dynamic> elementList;
  final Function(String) onLongPressTile;
  final Function(String) onClickTile;
  late _ListSecctionItemsWidgetState _listState;

  ListSecctionItemsWidget({
    Key? key,
    required this.elementList,
    required this.onLongPressTile,
    required this.onClickTile,
  }) : super(key: key);

  @override
  _ListSecctionItemsWidgetState createState() {
    _listState = _ListSecctionItemsWidgetState();
    return _listState;
  }

  void refreshList() {
    _listState.refreshList();
  }
}

class _ListSecctionItemsWidgetState extends State<ListSecctionItemsWidget> {
  late List<dynamic> _elementList;

  @override
  void initState() {
    super.initState();
    _elementList = widget.elementList;
  }

  void refreshList() {
    print("refreshList del widget");
    setState(() {
      _elementList = widget.elementList;
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _elementList.length,
      itemBuilder: (context, index) {
        final section = _elementList[index].categoryName;
        final items = _elementList[index].items;

        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: Colors.grey[200],
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  const Icon(Icons.folder),
                  const SizedBox(width: 8),
                  Text(
                    section,
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
            ),
            Column(
              children: items.map<Widget>((item) {
                return ListTile(
                  title: Text(
                    item.name,
                    style: const TextStyle(fontWeight: FontWeight.bold),
                  ),
                  subtitle: Text(
                    'Name: ${item.name}',
                    style: const TextStyle(fontSize: 12),
                  ),
                  onTap: () {
                    widget.onClickTile(item.id);
                  },
                  onLongPress: () {
                    widget.onLongPressTile(item.id);
                  },
                );
              }).toList(),
            ),
          ],
        );
      },
    );
  }
}
