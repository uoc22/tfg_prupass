import 'package:injector/injector.dart';
import 'package:prupass_androidstudio/data/datasources/fisestone/firestore_note_repository.dart';
import 'package:prupass_androidstudio/data/datasources/fisestone/firestore_password_repository.dart';
import 'package:prupass_androidstudio/data/datasources/fisestone/firestore_user_repository.dart';
import 'package:prupass_androidstudio/data/repositories/rsa/rsa_cryto_repository.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_note_repository.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_password_repository.dart';
import 'package:prupass_androidstudio/domain/usecases/passwords/usecases/delete_password_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/passwords/usecases/get_all_passwords_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/passwords/usecases/get_password_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/passwords/usecases/save_password_usecase.dart';
import 'package:prupass_androidstudio/ui/pages/login/blocs/user_bloc.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_crypto_repository.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_user_repository.dart';
import 'package:prupass_androidstudio/domain/usecases/login/usecases/create_user_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/login/usecases/get_user_usecase.dart';
import 'package:prupass_androidstudio/ui/pages/notes/blocs/notes_bloc.dart';
import 'package:prupass_androidstudio/domain/usecases/notes/usecases/delete_note_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/notes/usecases/get_all_notes_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/notes/usecases/get_note_usecase.dart';
import 'package:prupass_androidstudio/domain/usecases/notes/usecases/save_note_usecase.dart';
import 'package:prupass_androidstudio/ui/pages/passwords/blocs/passwords_bloc.dart';

class Register {
  void regist() {
    final injector = Injector.appInstance;

    // Repositorios de cryto
    injector.registerDependency<AbstractCrytoRepository>(() {
      return RSACryptoRepository();
    });

    // Repositorios de modelos
    injector.registerDependency<AbstractUserRepository>(() {
      return FirestoreUserRepository();
    });
    injector.registerDependency<AbstractNoteRepository>(() {
      return FirestoreNoteRepository();
    });
    injector.registerDependency<AbstractPasswordRepository>(() {
      return FirestorePasswordRepository();
    });

    // Casos de uso Users
    injector.registerDependency<CreateUserUseCase>(() {
      return CreateUserUseCase(injector.get<AbstractUserRepository>());
    });
    injector.registerDependency<GetUserUseCase>(() {
      return GetUserUseCase(injector.get<AbstractUserRepository>());
    });

    // Casos de uso Notes
    injector.registerDependency<GetAllNotesUseCase>(() {
      return GetAllNotesUseCase(injector.get<AbstractNoteRepository>());
    });
    injector.registerDependency<GetNoteUseCase>(() {
      return GetNoteUseCase(injector.get<AbstractNoteRepository>());
    });
    injector.registerDependency<SaveNoteUseCase>(() {
      return SaveNoteUseCase(injector.get<AbstractNoteRepository>());
    });
    injector.registerDependency<DeleteNoteUseCase>(() {
      return DeleteNoteUseCase(injector.get<AbstractNoteRepository>());
    });

    // Casos de uso Password
    injector.registerDependency<GetAllPasswordsUseCase>(() {
      return GetAllPasswordsUseCase(injector.get<AbstractPasswordRepository>());
    });
    injector.registerDependency<GetPasswordUseCase>(() {
      return GetPasswordUseCase(injector.get<AbstractPasswordRepository>());
    });
    injector.registerDependency<SavePasswordUseCase>(() {
      return SavePasswordUseCase(injector.get<AbstractPasswordRepository>());
    });
    injector.registerDependency<DeletePasswordUseCase>(() {
      return DeletePasswordUseCase(injector.get<AbstractPasswordRepository>());
    });

    // BLOCKS
    // Users
    injector.registerDependency<UserBloc>(
      () => UserBloc(
        injector.get<AbstractUserRepository>(),
        injector.get<CreateUserUseCase>(),
        injector.get<GetUserUseCase>(),
      ),
    );

    // Notes
    injector.registerDependency<NotesBloc>(
      () => NotesBloc(
        injector.get<AbstractNoteRepository>(),
        injector.get<GetAllNotesUseCase>(),
        injector.get<GetNoteUseCase>(),
        injector.get<SaveNoteUseCase>(),
        injector.get<DeleteNoteUseCase>(),
      ),
    );

    // Passwords
    injector.registerDependency<PasswordsBloc>(
      () => PasswordsBloc(
        injector.get<AbstractPasswordRepository>(),
        injector.get<GetAllPasswordsUseCase>(),
        injector.get<GetPasswordUseCase>(),
        injector.get<SavePasswordUseCase>(),
        injector.get<DeletePasswordUseCase>(),
      ),
    );
  }
}
