import 'dart:typed_data';

class UserEntity {
  final String id;
  final String email;
  final String name;
  final Uint8List password;
  final String lastname;
  final String? address;
  final String? phone;
  final String publickey;
  final String privatekey;

  UserEntity(
      {required this.id,
      required this.email,
      required this.name,
      required this.lastname,
      required this.password,
      this.address,
      this.phone,
      required this.publickey,
      required this.privatekey});
}
