class NoteEntity {
  String id;
  final String name;
  final String folder;
  final bool favorite;
  final String note;

  NoteEntity({
    required this.id,
    required this.name,
    required this.folder,
    required this.favorite,
    required this.note,
  });

  // Este es el constructor que maneja un Map<String, dynamic>
  NoteEntity.fromMap(Map<String, dynamic> map)
      : id = map['id'] ?? '',
        name = map['name'],
        folder = map['folder'],
        favorite = map['favorite'],
        note = map['note'] ?? '';

  factory NoteEntity.fromJson(Map<String, dynamic> json) {
    return NoteEntity(
      id: json['id'] as String ?? '',
      name: json['name'] as String ?? '',
      folder: json['folder'] as String ?? '',
      favorite: json['favorite'] as bool ?? false,
      note: json['note'] as String ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'folder': folder,
      'favorite': favorite.toString(),
      'note': note,
    };
  }
}

class NoteCategory {
  final String categoryName;
  final List<NoteEntity> items;

  NoteCategory({
    required this.categoryName,
    required this.items,
  });
}
