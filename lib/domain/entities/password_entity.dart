class PasswordEntity {
  String id;
  final String name;
  final String folder;
  final bool favorite;
  final String url;
  final String user;
  final String pass;
  final String note;

  PasswordEntity({
    required this.id,
    required this.name,
    required this.folder,
    required this.favorite,
    required this.url,
    required this.user,
    required this.pass,
    required this.note,
  });

  // Este es el constructor que maneja un Map<String, dynamic>
  PasswordEntity.fromMap(Map<String, dynamic> map)
      : id = map['id'] ?? '',
        name = map['name'],
        folder = map['folder'],
        favorite = map['favorite'],
        url = map['url'] ?? '',
        user = map['user'] ?? '',
        pass = map['pass'] ?? '',
        note = map['note'] ?? '';

  factory PasswordEntity.fromJson(Map<String, dynamic> json) {
    return PasswordEntity(
      id: json['id'] as String ?? '',
      name: json['name'] as String ?? '',
      folder: json['folder'] as String ?? '',
      favorite: json['favorite'] as bool ?? false,
      url: json['url'] as String ?? '',
      user: json['user'] as String ?? '',
      pass: json['pass'] as String ?? '',
      note: json['note'] as String ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'folder': folder,
      'favorite': favorite.toString(),
      'url': url,
      'user': user,
      'pass': pass,
      'note': note,
    };
  }
}

class VaultEntity {
  final String url;
  final String user;
  final String pass;
  final String note;

  VaultEntity({
    required this.url,
    required this.user,
    required this.pass,
    required this.note,
  });

  factory VaultEntity.fromJson(Map<String, dynamic> json) {
    return VaultEntity(
      url: json['url'] as String ?? '',
      user: json['user'] as String ?? '',
      pass: json['pass'] as String ?? '',
      note: json['note'] as String ?? '',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'url': url,
      'user': user,
      'pass': pass,
      'note': note,
    };
  }
}

class PasswordCategory {
  final String categoryName;
  final List<PasswordEntity> items;

  PasswordCategory({
    required this.categoryName,
    required this.items,
  });
}
