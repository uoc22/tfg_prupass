import 'package:injector/injector.dart';
import 'package:pointycastle/api.dart';
import 'package:prupass_androidstudio/data/repositories/crypto_repository_adapter.dart';
import 'package:prupass_androidstudio/domain/entities/note_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_crypto_repository.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_note_repository.dart';

class SaveNoteUseCase {
  final AbstractNoteRepository _repository;

  SaveNoteUseCase(this._repository);

  Future<NoteEntity?> saveNote(UserEntity user, String? id, String name,
      String folder, bool favorite, String note) async {
    CryptoRepositoryAdapter cr = CryptoRepositoryAdapter(
        Injector.appInstance.get<AbstractCrytoRepository>());
    PublicKey miKey = cr.publicKeyFromPem(user.publickey);

    var vault = cr.encryptPublicKey(note, miKey);
    // llamo al repositorio para guardar la nota
    return await _repository.saveNote(user, id, name, folder, favorite, vault);
  }
}
