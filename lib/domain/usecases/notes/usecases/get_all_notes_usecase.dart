import 'package:prupass_androidstudio/domain/entities/note_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_note_repository.dart';

class GetAllNotesUseCase {
  final AbstractNoteRepository _repository;

  GetAllNotesUseCase(this._repository);

  Future<List<NoteEntity>> getNotes(UserEntity user) async {
    // llamo al repositorio para obtener todas las notas del usuario
    print('llamamos al repositorio getNotes');
    return await _repository.getNotes(user);
  }
}
