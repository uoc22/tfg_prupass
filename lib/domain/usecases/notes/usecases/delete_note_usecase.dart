import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_note_repository.dart';

class DeleteNoteUseCase {
  final AbstractNoteRepository _repository;

  DeleteNoteUseCase(this._repository);

  Future<bool> deleteNote(UserEntity user, String id) async {
    // llamo al repositorio para eliminar la nota
    return await _repository.deleteNote(user, id);
  }
}
