import 'package:prupass_androidstudio/domain/entities/note_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_note_repository.dart';

class GetNoteUseCase {
  final AbstractNoteRepository _repository;

  GetNoteUseCase(this._repository);

  Future<NoteEntity?> getNote(UserEntity user, String id) async {
    // llamo al repositorio para obtener una nota
    return await _repository.getNote(user, id);
  }
}
