import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_user_repository.dart';

class CreateUserUseCase {
  final AbstractUserRepository _repository;

  CreateUserUseCase(this._repository);

  Future<UserEntity?> saveUser(String email, String password, String username,
      String lastname, String? address, String? phone) async {
    // llamo al repositorio para crear el usuario.
    return await _repository.register(
        email, password, username, lastname, address, phone);
  }
}
