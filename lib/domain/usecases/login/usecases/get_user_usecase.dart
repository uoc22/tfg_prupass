import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_user_repository.dart';

class GetUserUseCase {
  final AbstractUserRepository _repository;

  GetUserUseCase(this._repository);

  Future<UserEntity?> getUser(String email, String password) async {
    // llamo al repositorio para obtener el usuario y comprobar que es valido.
    // Al no guardarme la pass del usuario, lo que hago es descifrar el atributo privateKey
    // que esta cifrado con la password derivada, si soy capaz de descifrar la contraseña es correcta.
    return await _repository.login(email, password);
  }
}
