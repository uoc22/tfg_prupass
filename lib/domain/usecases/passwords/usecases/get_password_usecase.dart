import 'package:prupass_androidstudio/domain/entities/password_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_password_repository.dart';

class GetPasswordUseCase {
  final AbstractPasswordRepository _repository;

  GetPasswordUseCase(this._repository);

  Future<PasswordEntity?> getPassword(UserEntity user, String id) async {
    // llamo al repositorio para obtener una nota
    print("Llamamos al UC getPassword con id ${id}");

    return await _repository.getPassword(user, id);
  }
}
