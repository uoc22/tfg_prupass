import 'dart:convert';

import 'package:injector/injector.dart';
import 'package:pointycastle/api.dart';
import 'package:prupass_androidstudio/data/repositories/crypto_repository_adapter.dart';
import 'package:prupass_androidstudio/domain/entities/password_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_crypto_repository.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_password_repository.dart';

class SavePasswordUseCase {
  final AbstractPasswordRepository _repository;

  SavePasswordUseCase(this._repository);

  Future<PasswordEntity?> savePassword(
      UserEntity user,
      String? id,
      String name,
      String folder,
      bool favorite,
      String url,
      String userName,
      String pass,
      String note) async {
    CryptoRepositoryAdapter cr = CryptoRepositoryAdapter(
        Injector.appInstance.get<AbstractCrytoRepository>());
    PublicKey miKey = cr.publicKeyFromPem(user.publickey);

    VaultEntity valueEntity =
        VaultEntity(url: url, user: userName, pass: pass, note: note);
    String valueString = jsonEncode(valueEntity.toJson());

    var vault = cr.encryptPublicKey(valueString, miKey);
    // llamo al repositorio para guardar la nota
    return await _repository.savePassword(
        user, id, name, folder, favorite, vault);
  }
}
