import 'package:prupass_androidstudio/domain/entities/password_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_password_repository.dart';

class GetAllPasswordsUseCase {
  final AbstractPasswordRepository _repository;

  GetAllPasswordsUseCase(this._repository);

  Future<List<PasswordEntity>> getPasswords(UserEntity user) async {
    // llamo al repositorio para obtener todas las notas del usuario
    print('llamamos al repositorio getPasswords');
    return await _repository.getPasswords(user);
  }
}
