import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_password_repository.dart';

class DeletePasswordUseCase {
  final AbstractPasswordRepository _repository;

  DeletePasswordUseCase(this._repository);

  Future<bool> deletePassword(UserEntity user, String id) async {
    // llamo al repositorio para eliminar la nota
    return await _repository.deletePassword(user, id);
  }
}
