import "dart:typed_data";

import "package:pointycastle/export.dart";

abstract class AbstractCrytoRepository {
  // Asymetryc
  AsymmetricKeyPair<PublicKey, PrivateKey> generateKeyPair();

  String encryptPublicKey(String plainText, PublicKey publicKey);

  String decryptPrivateKey(String cipherText, PrivateKey privateKey);
  // Asymetryc

  // Symetryc
  Uint8List getDerivedPass(String password);

  String encrypt(String plainText, Uint8List key);

  String decrypt(String cipherText, Uint8List key);
  // Symetryc

  PublicKey publicKeyFromPem(pemString);

  PrivateKey privateKeyFromPem(pemString);

  String publicKeyToPem(PublicKey publicKey);

  String privateKeyToPem(PrivateKey privateKey);
}
