import 'package:prupass_androidstudio/domain/entities/note_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';

abstract class AbstractNoteRepository {
  Future<List<NoteEntity>> getNotes(UserEntity user);

  Future<NoteEntity?> getNote(UserEntity user, String id);

  Future<NoteEntity?> saveNote(UserEntity user, String? id, String name,
      String folder, bool favorite, String vault);

  Future<bool> deleteNote(UserEntity user, String id);
}
