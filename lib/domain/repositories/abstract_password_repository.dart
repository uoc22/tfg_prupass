import 'package:prupass_androidstudio/domain/entities/password_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';

abstract class AbstractPasswordRepository {
  Future<List<PasswordEntity>> getPasswords(UserEntity user);

  Future<PasswordEntity?> getPassword(UserEntity user, String id);

  Future<PasswordEntity?> savePassword(UserEntity user, String? id, String name,
      String folder, bool favorite, String vault);

  Future<bool> deletePassword(UserEntity user, String id);
}
