import 'dart:typed_data';

import 'package:prupass_androidstudio/domain/entities/user_entity.dart';

abstract class AbstractUserRepository {
  Future<UserEntity?> login(String email, String password);
  Future<UserEntity?> register(String email, String password, String name,
      String lastname, String? address, String? phone);
  Future<UserEntity?> update(
      String userId,
      String email,
      String name,
      String lastname,
      String? address,
      String? phone,
      String publickey,
      String privateke,
      Uint8List passderived);
}
