import 'dart:typed_data';

import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_user_repository.dart';

class UserRepositoryAdapter extends AbstractUserRepository {
  final AbstractUserRepository _repository;

  UserRepositoryAdapter(this._repository);

  @override
  Future<UserEntity?> login(String email, String password) {
    return _repository.login(email, password);
  }

  @override
  Future<UserEntity?> register(String email, String password, String name,
      String lastname, String? address, String? phone) {
    return _repository.register(
        email, password, name, lastname, address, phone);
  }

  @override
  Future<UserEntity?> update(
      String userId,
      String email,
      String name,
      String lastname,
      String? address,
      String? phone,
      String publickey,
      String privatekey,
      Uint8List passderived) {
    return _repository.update(userId, email, name, lastname, address, phone,
        publickey, privatekey, passderived);
  }
}
