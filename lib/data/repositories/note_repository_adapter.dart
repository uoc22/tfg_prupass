import 'package:prupass_androidstudio/domain/entities/note_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_note_repository.dart';

class NoteRepositoryAdapter extends AbstractNoteRepository {
  final AbstractNoteRepository _repository;

  NoteRepositoryAdapter(this._repository);

  @override
  Future<List<NoteEntity>> getNotes(UserEntity user) {
    return _repository.getNotes(user);
  }

  @override
  Future<NoteEntity?> getNote(UserEntity user, String id) {
    return _repository.getNote(user, id);
  }

  @override
  Future<NoteEntity?> saveNote(UserEntity user, String? id, String name,
      String folder, bool favorite, String vault) {
    return _repository.saveNote(user, id, name, folder, favorite, vault);
  }

  @override
  Future<bool> deleteNote(UserEntity user, String id) {
    return _repository.deleteNote(user, id);
  }
}
