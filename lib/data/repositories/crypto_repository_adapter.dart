import 'dart:typed_data';

import "package:pointycastle/export.dart";
import 'package:prupass_androidstudio/domain/repositories/abstract_crypto_repository.dart';

class CryptoRepositoryAdapter extends AbstractCrytoRepository {
  final AbstractCrytoRepository _repository;

  CryptoRepositoryAdapter(this._repository);

  @override
  AsymmetricKeyPair<PublicKey, PrivateKey> generateKeyPair() {
    return _repository.generateKeyPair();
  }

  @override
  String encryptPublicKey(String plaintext, PublicKey publicKey) {
    return _repository.encryptPublicKey(plaintext, publicKey as RSAPublicKey);
  }

  @override
  String decryptPrivateKey(String cipherText, PrivateKey privateKey) {
    return _repository.decryptPrivateKey(
        cipherText, privateKey as RSAPrivateKey);
  }

  @override
  PublicKey publicKeyFromPem(pemString) {
    return _repository.publicKeyFromPem(pemString);
  }

  @override
  String publicKeyToPem(PublicKey publicKey) {
    return _repository.publicKeyToPem(publicKey);
  }

  @override
  PrivateKey privateKeyFromPem(pemString) {
    return _repository.privateKeyFromPem(pemString);
  }

  @override
  String privateKeyToPem(PrivateKey privateKey) {
    return _repository.privateKeyToPem(privateKey);
  }

  @override
  Uint8List getDerivedPass(String password) {
    return _repository.getDerivedPass(password);
  }

  @override
  String encrypt(String plainText, Uint8List key) {
    return _repository.encrypt(plainText, key);
  }

  @override
  String decrypt(String cipherText, Uint8List key) {
    return _repository.decrypt(cipherText, key);
  }
}
