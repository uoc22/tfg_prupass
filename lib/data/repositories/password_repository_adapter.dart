import 'package:prupass_androidstudio/domain/entities/password_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_password_repository.dart';

class PasswordRepositoryAdapter extends AbstractPasswordRepository {
  final AbstractPasswordRepository _repository;

  PasswordRepositoryAdapter(this._repository);

  @override
  Future<List<PasswordEntity>> getPasswords(UserEntity user) {
    return _repository.getPasswords(user);
  }

  @override
  Future<PasswordEntity?> getPassword(UserEntity user, String id) {
    return _repository.getPassword(user, id);
  }

  @override
  Future<PasswordEntity?> savePassword(UserEntity user, String? id, String name,
      String folder, bool favorite, String vault) {
    return _repository.savePassword(user, id, name, folder, favorite, vault);
  }

  @override
  Future<bool> deletePassword(UserEntity user, String id) {
    return _repository.deletePassword(user, id);
  }
}
