import 'dart:developer';
import 'dart:typed_data';

import 'package:injector/injector.dart';
import 'package:pointycastle/api.dart';
import 'package:pointycastle/asymmetric/api.dart';
import 'package:prupass_androidstudio/data/models/firesote_user.dart';
import 'package:prupass_androidstudio/data/repositories/crypto_repository_adapter.dart';
import 'package:prupass_androidstudio/data/services/firesotre_user_mapper.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_crypto_repository.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_user_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FirestoreUserRepository extends AbstractUserRepository {
  FirebaseFirestore db = FirebaseFirestore.instance;

  @override
  Future<UserEntity?> login(String email, String password) async {
    // Obtenemos el usurio por el email, y comprobamos si podemos descifrar su privateKey con la password
    DocumentSnapshot? document = await getUserByEmail(email);
    if (document == null) {
      return null;
    }

    // derivamos la clave
    CryptoRepositoryAdapter cr = CryptoRepositoryAdapter(
        Injector.appInstance.get<AbstractCrytoRepository>());
    Uint8List passderived = cr.getDerivedPass(password);

    // Comprobar que con la contraseña puede leer la privateKey, sino salimos
    String plainPrivateKey = cr.decrypt(document['privatekey'], passderived);
    if (plainPrivateKey.isEmpty) {
      return null;
    }

    final firestoreUser = FirestoreUser(
      id: document.id,
      email: document['email'],
      name: document['name'],
      lastname: document['lastname'] ?? '',
      address: document['address'] ?? '',
      phone: document['phone'] ?? '',
      publickey: document['publickey'],
      privatekey: plainPrivateKey,
    );

    return firestoreUser.toUserEntity(passderived);
  }

  @override
  Future<UserEntity?> register(String email, String password, String name,
      String lastname, String? address, String? phone) async {
    DocumentSnapshot? document = await getUserByEmail(email);
    if (document != null) {
      return null;
    }

    // derivamos la clave
    CryptoRepositoryAdapter cr = CryptoRepositoryAdapter(
        Injector.appInstance.get<AbstractCrytoRepository>());
    Uint8List passderived = cr.getDerivedPass(password);

    // Generar las claves privada y publica
    AsymmetricKeyPair<PublicKey, PrivateKey> keys = cr.generateKeyPair();
    final String publicKeyString =
        cr.publicKeyToPem(keys.publicKey as RSAPublicKey);
    final String privateKeyString =
        cr.privateKeyToPem(keys.privateKey as RSAPrivateKey);

    final firestoreUser = FirestoreUser(
      id: '',
      email: email,
      name: name,
      lastname: lastname,
      address: address,
      phone: phone,
      publickey: publicKeyString,
      privatekey: privateKeyString,
    );

    final midoc = firestoreUser.toDocument(passderived);

    // Guardar el usuario en Firestore
    await db.collection('users').add(midoc);

    return firestoreUser.toUserEntity(passderived);
  }

  @override
  Future<UserEntity?> update(
      String userId,
      String email,
      String name,
      String lastname,
      String? address,
      String? phone,
      String publickey,
      String privatekey,
      Uint8List passderived) async {
    // Si no existe el documento, salimos
    DocumentSnapshot? document = await getUserByEmail(email);
    if (document == null) {
      return null;
    }

    // podemos modificarlo
    final firestoreUser = FirestoreUser(
      id: userId,
      email: email,
      name: name,
      lastname: lastname,
      address: address,
      phone: phone,
      publickey: publickey,
      privatekey: privatekey,
    );

    // Guardar el usuario en Firestore
    await db
        .collection('users')
        .doc(firestoreUser.id)
        .set(firestoreUser.toDocument(passderived));

    return firestoreUser.toUserEntity(passderived);
  }

  Future<DocumentSnapshot?> getUserByEmail(String email) async {
    final querySnapshot =
        await db.collection('users').where('email', isEqualTo: email).get();
    if (querySnapshot.docs.isEmpty) {
      return null;
    }
    return querySnapshot.docs.first;
  }

  Future<List> getUsers() async {
    List result = [];

    CollectionReference collectionReference = db.collection('users');
    QuerySnapshot query = await collectionReference.get();

    for (var user in query.docs) {
      result.add(user.data());
    }

    return result;
  }
}
