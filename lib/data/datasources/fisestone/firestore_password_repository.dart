import 'dart:developer';

import 'package:prupass_androidstudio/data/models/firesote_password.dart';
import 'package:prupass_androidstudio/data/services/firesotre_password_mapper.dart';
import 'package:prupass_androidstudio/domain/entities/password_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_password_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FirestorePasswordRepository extends AbstractPasswordRepository {
  FirebaseFirestore db = FirebaseFirestore.instance;
  // Las notas seran del usuario logueado

  @override
  Future<List<PasswordEntity>> getPasswords(UserEntity user) async {
    List<PasswordEntity> result = [];
    // obtenemos la referencia al usuario en cuestion
    final userRef = db.collection('users').doc(user.id);

    // obtenemos toda la coleccion de notas
    final query = await userRef.collection('passwords').get();

    for (var doc in query.docs) {
      var firestorePassword = FirestorePassword.fromMap(doc.data());
      firestorePassword.id = doc.id; // Setear el ID del elemento de Firebase
      var passwordEntity = firestorePassword.toPasswordEntity(user);
      result.add(passwordEntity);
    }

    return result;
  }

  @override
  Future<PasswordEntity?> getPassword(UserEntity user, String id) async {
    // obtenemos la referencia al usuario en cuestion
    final userRef = db.collection('users').doc(user.id);

    // obtenemos la referencia a la nota
    final doc = userRef.collection('passwords').doc(id);

    var snapshot = await doc.get();
    if (snapshot.exists) {
      print("Si id ${id}");
      var data = snapshot.data();
      if (data != null) {
        var firestorePassword = FirestorePassword.fromMap(data);
        firestorePassword.id = doc.id; // Setear el ID del elemento de Firebase
        var passwordEntity = firestorePassword.toPasswordEntity(user);
        return passwordEntity;
      }
    }
    print("no hemos encontrado el id ${id}");
    return null;
  }

  @override
  Future<PasswordEntity?> savePassword(UserEntity user, String? id, String name,
      String folder, bool favorite, String vault) async {
    // obtenemos la referencia al usuario en cuestion
    final userRef = db.collection('users').doc(user.id);

    //instanciamos mi firestoreNote
    final firestorePassword = FirestorePassword(
      id: id ?? '',
      name: name,
      folder: folder,
      favorite: favorite,
      vault: vault,
    );

    // lo transformamos en un document
    final doc = firestorePassword.toDocument();

    // Guardar la nota en Firestore
    if (id == null || id.isEmpty) {
      var newDoc = await userRef.collection('passwords').add(doc);
      firestorePassword.id = newDoc.id; // Setear el ID del elemento de Firebase
    } else {
      await userRef.collection('passwords').doc(id).set(doc);
    }

    // Retornamos el NoteEntity actualizado
    return firestorePassword.toPasswordEntity(user);
  }

  @override
  Future<bool> deletePassword(UserEntity user, String id) async {
    try {
      // obtenemos la referencia al usuario en cuestion
      final userRef = db.collection('users').doc(user.id);

      // Eliminamos la nota de la colección de notas del usuario
      await userRef.collection('passwords').doc(id).delete();

      return true;
    } catch (e) {
      // No hacer nada
    }
    return false;
  }
}
