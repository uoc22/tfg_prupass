import 'dart:developer';

import 'package:prupass_androidstudio/data/models/firesote_note.dart';
import 'package:prupass_androidstudio/data/services/firesotre_note_mapper.dart';
import 'package:prupass_androidstudio/domain/entities/note_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_note_repository.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FirestoreNoteRepository extends AbstractNoteRepository {
  FirebaseFirestore db = FirebaseFirestore.instance;
  // Las passwords seran del usuario logueado

  @override
  Future<List<NoteEntity>> getNotes(UserEntity user) async {
    List<NoteEntity> result = [];
    // obtenemos la referencia al usuario en cuestion
    final userRef = db.collection('users').doc(user.id);

    // obtenemos toda la coleccion de notas
    final query = await userRef.collection('notes').get();

    for (var doc in query.docs) {
      var firestoreNote = FirestoreNote.fromMap(doc.data());
      firestoreNote.id = doc.id; // Setear el ID del elemento de Firebase
      var noteEntity = firestoreNote.toNoteEntity(user);
      result.add(noteEntity);
    }
    /*
    // Imprimir el resultado en la consola
    result.forEach((noteEntity) {
      print("Item2: ${noteEntity.toJson()}");
    });
    */
    return result;
  }

  @override
  Future<NoteEntity?> getNote(UserEntity user, String id) async {
    // obtenemos la referencia al usuario en cuestion
    final userRef = db.collection('users').doc(user.id);

    // obtenemos la referencia a la nota
    final doc = userRef.collection('notes').doc(id);

    var snapshot = await doc.get();
    if (snapshot.exists) {
      var data = snapshot.data();
      if (data != null) {
        var firestoreNote = FirestoreNote.fromMap(data);
        firestoreNote.id = snapshot.id; // Setear el ID del elemento de Firebase
        var noteEntity = firestoreNote.toNoteEntity(user);
        return noteEntity;
      }
    }

    return null;
  }

  @override
  Future<NoteEntity?> saveNote(UserEntity user, String? id, String name,
      String folder, bool favorite, String vault) async {
    // obtenemos la referencia al usuario en cuestion
    final userRef = db.collection('users').doc(user.id);

    //instanciamos mi firestoreNote
    final firestoreNote = FirestoreNote(
      id: id ?? '',
      name: name,
      folder: folder,
      favorite: favorite,
      vault: vault,
    );

    // lo transformamos en un document
    final doc = firestoreNote.toDocument();

    // Guardar la nota en Firestore
    if (id == null || id.isEmpty) {
      var newDoc = await userRef.collection('notes').add(doc);
      firestoreNote.id = newDoc.id; // Setear el ID del elemento de Firebase
    } else {
      await userRef.collection('notes').doc(id).set(doc);
    }

    // Retornamos el NoteEntity actualizado
    return firestoreNote.toNoteEntity(user);
  }

  @override
  Future<bool> deleteNote(UserEntity user, String id) async {
    try {
      // obtenemos la referencia al usuario en cuestion
      final userRef = db.collection('users').doc(user.id);

      // Eliminamos la nota de la colección de notas del usuario
      await userRef.collection('notes').doc(id).delete();

      return true;
    } catch (e) {
      // No hacer nada
    }
    return false;
  }
}
