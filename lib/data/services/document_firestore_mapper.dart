import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:prupass_androidstudio/domain/entities/note_entity.dart';

extension NoteMapper on DocumentReference {
  Future<NoteEntity> toNoteEntity() async {
    String id = this.id;
    Map<String, dynamic> data = ((await get()).data()!) as Map<String, dynamic>;

    String name = data['name'];
    String folder = data['folder'];
    bool favorite = data['favorite'];
    String vault = data['vault'];

    return NoteEntity(
      id: id,
      name: name,
      folder: folder,
      favorite: favorite,
      note: vault,
    );
  }
}
