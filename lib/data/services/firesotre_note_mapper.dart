import 'dart:typed_data';

import 'package:injector/injector.dart';
import 'package:pointycastle/api.dart';
import 'package:prupass_androidstudio/data/models/firesote_note.dart';
import 'package:prupass_androidstudio/data/repositories/crypto_repository_adapter.dart';
import 'package:prupass_androidstudio/domain/entities/note_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_crypto_repository.dart';

extension NoteMapper on FirestoreNote {
  NoteEntity toNoteEntity(UserEntity user) {
    CryptoRepositoryAdapter cr = CryptoRepositoryAdapter(
        Injector.appInstance.get<AbstractCrytoRepository>());

    PrivateKey miKey = cr.privateKeyFromPem(user.privatekey);
    var note = cr.decryptPrivateKey(vault, miKey);
    print("note: ${note}");

    return NoteEntity(
      id: id,
      name: name,
      folder: folder,
      favorite: favorite,
      note: note,
    );
  }

  Map<String, dynamic> toDocument() {
    final document = {
      'name': name,
      'folder': folder,
      'favorite': favorite,
      'vault': vault,
    };
    if (id.isNotEmpty) {
      document['id'] = id;
    }

    return document;
  }
}
