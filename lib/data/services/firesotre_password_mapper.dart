import 'dart:convert';

import 'package:injector/injector.dart';
import 'package:pointycastle/api.dart';
import 'package:prupass_androidstudio/data/models/firesote_password.dart';
import 'package:prupass_androidstudio/data/repositories/crypto_repository_adapter.dart';
import 'package:prupass_androidstudio/domain/entities/password_entity.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_crypto_repository.dart';

extension PasswordMapper on FirestorePassword {
  PasswordEntity toPasswordEntity(UserEntity user) {
    CryptoRepositoryAdapter cr = CryptoRepositoryAdapter(
        Injector.appInstance.get<AbstractCrytoRepository>());

    PrivateKey miKey = cr.privateKeyFromPem(user.privatekey);
    Map<String, dynamic> vaultJSON =
        jsonDecode(cr.decryptPrivateKey(vault, miKey));
    VaultEntity vaultEntity = VaultEntity.fromJson(vaultJSON);

    return PasswordEntity(
      id: id,
      name: name,
      folder: folder,
      favorite: favorite,
      url: vaultEntity.url,
      user: vaultEntity.user,
      pass: vaultEntity.pass,
      note: vaultEntity.note,
    );
  }

  Map<String, dynamic> toDocument() {
    final document = {
      'name': name,
      'folder': folder,
      'favorite': favorite,
      'vault': vault,
    };
    if (id.isNotEmpty) {
      document['id'] = id;
    }

    return document;
  }
}
