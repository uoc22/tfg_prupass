import 'package:injector/injector.dart';
import 'package:prupass_androidstudio/data/models/firesote_user.dart';
import 'package:prupass_androidstudio/data/repositories/crypto_repository_adapter.dart';
import 'package:prupass_androidstudio/domain/entities/user_entity.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_crypto_repository.dart';

extension UserMapper on FirestoreUser {
  UserEntity toUserEntity(final passderived) {
    return UserEntity(
      id: id,
      name: name,
      password: passderived,
      email: email,
      lastname: lastname,
      address: address,
      phone: phone,
      publickey: publickey,
      privatekey: privatekey,
    );
  }

  Map<String, dynamic> toDocument(final passderived) {
    CryptoRepositoryAdapter cr = CryptoRepositoryAdapter(
        Injector.appInstance.get<AbstractCrytoRepository>());

    // Antes de retornar un documento para firesore, tenemos que cifrar la clave privada
    final cipherPrivateKey = cr.encrypt(privatekey, passderived);

    final document = {
      'email': email,
      'name': name,
      'lastname': lastname,
      'address': address,
      'phone': phone,
      'publickey': publickey,
      'privatekey': cipherPrivateKey,
    };
    if (id.isNotEmpty) {
      document['id'] = id;
    }

    return document;
  }
}
