import 'package:prupass_androidstudio/data/services/firesotre_note_mapper.dart';

class FirestoreNote {
  String id;
  final String name;
  final String folder;
  final bool favorite;
  final String vault;

  FirestoreNote({
    required this.id,
    required this.name,
    required this.folder,
    required this.favorite,
    required this.vault,
  });

  FirestoreNote.fromMap(Map<String, dynamic> map)
      : id = map['id'] ?? '',
        name = map['name'],
        folder = map['folder'],
        favorite = map['favorite'],
        vault = map['vault'] ?? '';
}
