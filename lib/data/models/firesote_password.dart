import 'package:prupass_androidstudio/data/services/firesotre_password_mapper.dart';

class FirestorePassword {
  String id;
  final String name;
  final String folder;
  final bool favorite;
  final String vault;

  FirestorePassword({
    required this.id,
    required this.name,
    required this.folder,
    required this.favorite,
    required this.vault,
  });

  FirestorePassword.fromMap(Map<String, dynamic> map)
      : id = map['id'] ?? '',
        name = map['name'],
        folder = map['folder'],
        favorite = map['favorite'],
        vault = map['vault'] ?? '';
}
