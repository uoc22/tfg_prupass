import 'package:prupass_androidstudio/data/services/firesotre_user_mapper.dart';

class FirestoreUser {
  final String id;
  final String email;
  final String name;
  final String lastname;
  final String? address;
  final String? phone;
  final String publickey;
  final String privatekey;

  FirestoreUser(
      {required this.id,
      required this.email,
      required this.name,
      required this.lastname,
      required this.address,
      required this.phone,
      required this.publickey,
      required this.privatekey});
}
