import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:prupass_androidstudio/di/reposiroties_register.dart';
import 'package:prupass_androidstudio/ui/pages/configuration/account_configuration_screen.dart';
import 'package:prupass_androidstudio/ui/pages/configuration/configuration_screen.dart';
import 'package:prupass_androidstudio/ui/pages/configuration/language_configuration_screen.dart';
import 'package:prupass_androidstudio/ui/pages/configuration/security_configuration_screen.dart';
import 'package:prupass_androidstudio/ui/pages/home_screen.dart';
import 'package:prupass_androidstudio/ui/pages/login/create_user_screen.dart';
import 'package:prupass_androidstudio/ui/pages/login/login_screen.dart';
import 'package:prupass_androidstudio/ui/pages/my_home_page.dart';
import 'package:injector/injector.dart';
import 'package:provider/provider.dart';
import 'package:prupass_androidstudio/ui/pages/notes/edit_notes_screen.dart';
import 'package:prupass_androidstudio/ui/pages/notes/list_notes_screen.dart';
import 'package:prupass_androidstudio/ui/pages/passwords/edit_passwords_screen.dart';
import 'package:prupass_androidstudio/ui/pages/passwords/list_passwords_screen.dart';
import 'package:prupass_androidstudio/ui/providers/user_provider.dart';

import 'firebase_options.dart';

void main() async {
  // instanciamos el inyector de dependencias
  Register().regist();

  // Inicializar Firebase
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(ChangeNotifierProvider(
    create: (_) => UserProvider(),
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Prupass',
      theme: ThemeData(
        primarySwatch: Colors.purple,
      ),
      home: const HomeScreen(),
      routes: {
        '/home_screen': (context) => HomeScreen(),
        '/login_screen': (context) => LoginScreen(),
        '/create_user_screen': (context) => CreateUserScreen(),
        '/list_passwords_screen': (context) => ListPasswordsScreen(),
        '/edit_passwords_screen': (context) => EditPasswordsScreen(
            id: ModalRoute.of(context)!.settings.arguments as String?),
        '/list_notes_screen': (context) => ListNotesScreen(),
        '/edit_notes_screen': (context) => EditNotesScreen(
            noteId: ModalRoute.of(context)!.settings.arguments as String?),
        '/configuration_screen': (context) => ConfigurationScreen(),
        '/account_configuration_screen': (context) =>
            AccountConfigurationScreen(),
        '/security_configuration_screen': (context) =>
            SecurityConfigurationScreen(),
        '/language_configuration_screen': (context) =>
            LanguageConfigurationScreen(),
      },
    );
  }
}
