import 'dart:convert';

String convertListToBase64(List<int> data) {
  final base64String = base64Encode(data);
  return base64String;
}

String convertListToString(List<int> data) {
  final stringData = String.fromCharCodes(data);
  return stringData;
}

List<int> convertStringToList(String data) {
  final listData = data.codeUnits;
  return listData;
}
