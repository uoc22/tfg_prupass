import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:injector/injector.dart';
import 'package:pointycastle/pointycastle.dart';
import 'package:prupass_androidstudio/data/repositories/crypto_repository_adapter.dart';
import 'package:prupass_androidstudio/data/repositories/rsa/rsa_cryto_repository.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_crypto_repository.dart';

void main() {
  testWidgets('Encrypt and decrypt', (WidgetTester tester) async {
    AbstractCrytoRepository myCrypto = RSACryptoRepository();

    String text = "Texto de prueba";
    String pass = "Patata";
    Uint8List key = myCrypto.getDerivedPass(pass);

    String cypher = myCrypto.encrypt(text, key);

    String plainText = myCrypto.decrypt(cypher, key);

    expect(text, equals(plainText));
  });

  testWidgets('Check asymmetric cryptography', (WidgetTester tester) async {
    final cr = RSACryptoRepository();
    final serviceCrypto = CryptoRepositoryAdapter(cr);

    // Generar par de claves
    final keyPair = serviceCrypto.generateKeyPair();
    final publicKey = keyPair.publicKey;
    final privateKey = keyPair.privateKey;

    // Texto original
    const plaintext = '¡Hola, mundo!';

    // Cifrar utilizando clave pública
    final encrypted = serviceCrypto.encryptPublicKey(plaintext, publicKey);

    // Descifrar utilizando clave privada
    final decrypted = serviceCrypto.decryptPrivateKey(encrypted, privateKey);

    expect(plaintext, equals(decrypted));
  });

  testWidgets('Check PEM format generation and reading',
      (WidgetTester tester) async {
    final cr = RSACryptoRepository();
    final serviceCrypto = CryptoRepositoryAdapter(cr);

    // Generar par de claves
    final keyPair = serviceCrypto.generateKeyPair();
    final publicKey = keyPair.publicKey;
    final privateKey = keyPair.privateKey;

    // Texto original
    const plaintext = '¡Hola, mundo!';

    // Cifrar utilizando clave pública
    final encrypted = serviceCrypto.encryptPublicKey(plaintext, publicKey);

    // Descifrar utilizando clave privada
    final decrypted = serviceCrypto.decryptPrivateKey(encrypted, privateKey);

    final String publicKeyString =
        serviceCrypto.publicKeyToPem(publicKey as RSAPublicKey);
    final String privateKeyString =
        serviceCrypto.privateKeyToPem(privateKey as RSAPrivateKey);

    final publicKey2 = serviceCrypto.publicKeyFromPem(publicKeyString);
    final privateKey2 = serviceCrypto.privateKeyFromPem(privateKeyString);

    expect(publicKey, equals(publicKey2));
    expect(privateKey2, equals(privateKey2));

    // Descifrar utilizando clave privada
    final decrypted2 = serviceCrypto.decryptPrivateKey(encrypted, privateKey2);
    final encrypted3 = serviceCrypto.encryptPublicKey(plaintext, publicKey2);
    final decrypted3 = serviceCrypto.decryptPrivateKey(encrypted3, privateKey);

    expect(decrypted2, equals(decrypted3));
  });
}
