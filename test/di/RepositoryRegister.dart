import 'dart:typed_data';

import 'package:flutter_test/flutter_test.dart';
import 'package:injector/injector.dart';
import 'package:pointycastle/pointycastle.dart';
import 'package:prupass_androidstudio/data/repositories/crypto_repository_adapter.dart';
import 'package:prupass_androidstudio/data/repositories/rsa/rsa_cryto_repository.dart';
import 'package:prupass_androidstudio/domain/repositories/abstract_crypto_repository.dart';

void main() {
  testWidgets('Inject repository requesting an abstrac',
      (WidgetTester tester) async {
    AbstractCrytoRepository myCrypto = RSACryptoRepository();

    String text = "Texto de prueba";
    String pass = "Patata";
    Uint8List key = myCrypto.getDerivedPass(pass);

    String cypher = myCrypto.encrypt(text, key);

    String plainText = myCrypto.decrypt(cypher, key);

    expect(text, equals(plainText));
  });
}
